FROM python:3.6-alpine
ADD . /gists
WORKDIR /gists
RUN pip install -r requirements.txt
CMD ["python", "work.py"]

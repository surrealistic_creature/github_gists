from pymongo import MongoClient
from pymongo.errors import BulkWriteError
import json, requests, pprint, time, datetime, os

client = MongoClient('mongo', 27017)
db = client.catapodt
posts = db.posts
history = db.history
posts.create_index('id', unique=True)

def log_and_history(count):
    count_data = {'count': count, 'datetime': datetime.datetime.now()}
    write_history = history.insert_one(count_data)
    print(count_data)


def dbwriteposts(gists):
    try:
        posts_ids = posts.insert_many(gists, ordered=False).inserted_ids
        log_and_history(len(posts_ids))
    except BulkWriteError as e:
        log_and_history(e.details.get('nInserted'))

def fetch_gists_and_save(secret, per_page):
    headers = {'Authorization':'token {}'.format(secret)}
    response = requests.get('https://api.github.com/gists/public?page=1&per_page={}'.format(per_page), headers=headers)
    gists = response.json()
    if type(gists) is not list:
        print(gists)
        return
    if type(gists[0]) is not dict:
        print(gists)
        return
    for gist in gists:
        files_list = list(gist.get('files').values())
        gist.update({'files': files_list})
    dbwriteposts(gists)


if __name__ == '__main__':
    secret = os.environ.get('GITHUB_TOKEN')
    per_page = os.environ.get('PER_PAGE')
    interval = os.environ.get('INTERVAL')
    while True:
        fetch_gists_and_save(secret, per_page)
        time.sleep(int(interval))

